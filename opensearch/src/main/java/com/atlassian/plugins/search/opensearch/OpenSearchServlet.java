package com.atlassian.plugins.search.opensearch;

import com.atlassian.plugins.search.SearchResultsMarshaller;
import com.atlassian.plugins.search.SearchResultsMarshallerFactory;
import com.atlassian.plugins.util.TokenExtractor;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.search.SearchProvider;
import com.atlassian.sal.api.search.SearchResults;
import com.atlassian.sal.api.user.UserManager;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;

import static com.atlassian.plugins.util.Check.notNull;


/**
 * Servlet to provide search results as a feed.
 */
public class OpenSearchServlet extends HttpServlet
{
    private static final Logger log = LoggerFactory.getLogger(OpenSearchServlet.class);

    private SearchProvider searchProvider;
    private UserManager userManager;
    private ApplicationProperties applicationProperties;
    private SearchResultsMarshallerFactory searchResultsMarshallerFactory;

    public static final String QUERY_PARAM = "query";
    public static final String START_PARAM = "start";
    public static final String COUNT_PARAM = "count";
    public static final String FEED_TYPE_PARAM = "format";
    public static final String SHOULD_IMPERSONATE_PARAM = "impersonation";
    public static final String IMPERSONATE_USER_PARAM = "user";
    public static final String CONTENTSIZE_PARAM = "content";

    public static final String DEFAULT_COUNT = "10";
    public static final String DEFAULT_FEED_TYPE = "rss_2.0";
    public static final String SEARCH_URL_ENCODING = "UTF-8";

    public OpenSearchServlet(SearchProvider searchProvider, UserManager userManager, ApplicationProperties applicationProperties, SearchResultsMarshallerFactory searchResultsMarshallerFactory)
    {
        this.searchProvider = notNull(searchProvider, "searchProvider");
        this.userManager = notNull(userManager, "userManager");
        this.applicationProperties = notNull(applicationProperties, "applicationProperties");
        this.searchResultsMarshallerFactory = notNull(searchResultsMarshallerFactory, "searchResultsMarshallerFactory");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        Map<String, String> parameters = extractParams(req);
        if (log.isDebugEnabled())
        {
            StringBuilder builder = new StringBuilder();
            builder.append("Incoming OpenSearch query parameters: ");
            for (Map.Entry<String, String> param : parameters.entrySet())
            {
                builder.append(param.getKey()).append("=").append(param.getValue()).append(" | ");
            }
            log.debug(builder.toString());
        }
        String query = extractParam(parameters, QUERY_PARAM, null, true);

        String currentUser = userManager.getRemoteUsername();
        boolean shouldImpersonate = extractParam(parameters, SHOULD_IMPERSONATE_PARAM, false, false);
        String userToImpersonate = extractParam(parameters, IMPERSONATE_USER_PARAM, "", false);
        int contentSize = tryParseInt(extractParam(parameters, CONTENTSIZE_PARAM, "0", false), 0);

        if (shouldImpersonate && !StringUtils.isBlank(userToImpersonate) && StringUtils.isBlank(currentUser))
        {
            final String msg = String.format("Anonymous search request is not permitted to impersonate user %s - try authenticating first.", userToImpersonate);

            // An anonymous request is not allowed to impersonate any user for searching. However, instead of throwing
            // a security exception, instead let's encourage the client to authenticate properly before trying again.
            resp.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            resp.setContentType("text/plain");

            PrintWriter writer = resp.getWriter();
            writer.write(msg);
            writer.close();

            log.error(msg);
            return;
        }
        String user = getSearchUser(shouldImpersonate, userToImpersonate, currentUser, resp);
        String queryUrl = buildSearchUrl(query, parameters);

        log.debug(String.format("Running OpenSearch query %s for user %s", queryUrl, user));
        SearchResults results = searchProvider.search(user, queryUrl);
        log.debug(String.format("OpenSearch query completed. Found %s results (search time: %s)", results.getTotalResults(), results.getSearchTime()));

        String feedType = extractParam(parameters, FEED_TYPE_PARAM, DEFAULT_FEED_TYPE, true);
        log.debug(String.format("Marshalling search results to %s", feedType));

        SearchResultsMarshaller marshaller = searchResultsMarshallerFactory.create(feedType, contentSize);
        resp.setContentType(getContentType(feedType));
        marshaller.marshalTo(results, applicationProperties.getBaseUrl(), query, resp.getWriter());
    }

    /**
     * Gets the user which the search should be performed under. By default this will be the authenticated user, or
     * an impersonate parameter if sent by an admin user.
     *
     * @param resp       the servlet response
     * @return the user to perform the search for
     * @throws IllegalArgumentException if the user to impersonate does not exist
     * @throws SecurityException        if a non-admin user attempts to impersonate someone
     */
    protected String getSearchUser(boolean shouldImpersonate, String impersonateUsername, String remoteUsername, HttpServletResponse resp) throws IllegalArgumentException, SecurityException
    {
        if (!shouldImpersonate)
        {
            log.debug("Search query did not request impersonation - executing search as " + remoteUsername);
            return remoteUsername;
        }

        if (StringUtils.isBlank(impersonateUsername))
        {
            log.debug("Search query requested impersonation but did not specify a username - impersonating the anonymous user context.");
            return "";
        }

        if (!userManager.isSystemAdmin(remoteUsername))
        {
            resp.setStatus(HttpServletResponse.SC_FORBIDDEN);
            String msg = String.format("%s cannot be impersonated because %s is not a System Administrator", impersonateUsername, remoteUsername);
            log.error(msg);
            throw new SecurityException(msg);
        }

        if (userManager.resolve(impersonateUsername) == null)
        {
            log.warn(String.format("%s requested to impersonate %s, but that user does not exist. Impersonating the anonymous user context instead.", remoteUsername, impersonateUsername));
            return "";
        }

        log.debug(String.format("%s has been permitted to impersonate %s", remoteUsername, impersonateUsername));
        return impersonateUsername;
    }

    /**
     * Creates a search URL to be passed to SAL
     *
     * @param query      the search query
     * @param parameters request parameters
     * @return a URL-encoded search string
     */
    protected String buildSearchUrl(String query, Map<String, String> parameters)
    {
        String count = extractParam(parameters, COUNT_PARAM, DEFAULT_COUNT, true);
        String start = extractParam(parameters, START_PARAM, "0", true);

        try
        {
            return MessageFormat.format("{0}&startIndex={1}&maxhits={2}",
                    URLEncoder.encode(query, SEARCH_URL_ENCODING),
                    URLEncoder.encode(start, SEARCH_URL_ENCODING),
                    URLEncoder.encode(count, SEARCH_URL_ENCODING));
        }
        catch (UnsupportedEncodingException uee)
        {
            //this should never happen
            throw new RuntimeException("Failed to encode query URL", uee);
        }
    }

    protected boolean extractParam(Map<String, String> req, String key, boolean defaultValue, boolean required)
    {
        return Boolean.parseBoolean(extractParam(req, key, Boolean.toString(defaultValue), required));
    }

    /**
     * Extracts a string parameter from a servlet request. Does not allow blank parameters.
     *
     * @param req          the servlet request
     * @param key          the key to use to find the parameter
     * @param defaultValue a default value to use if the parameter cannot be found or is blank
     * @return the parameter value
     */
    protected String extractParam(Map<String, String> req, String key, String defaultValue, boolean required)
    {
        String supplied = req.get(key);

        if (!StringUtils.isBlank(supplied))
            return supplied;
        else if (defaultValue != null)
            return defaultValue;
        else if (required)
            throw new IllegalArgumentException("Parameter " + key + " must be supplied");
        else
            return null;
    }

    private Map<String, String> extractParams(HttpServletRequest request)
    {
        //extract standard querystring params, and special $$key=value$$ params from the query
        Map<String, String> rv = new HashMap<String, String>();

        Map<String, String[]> requestParamMap = request.getParameterMap();

        for (String key : requestParamMap.keySet())
            rv.put(key, requestParamMap.get(key)[0]);

        String query = rv.get(QUERY_PARAM);

        TokenExtractor extractor = new TokenExtractor(query);

        rv.put(QUERY_PARAM, extractor.getRemaining());

        //we disallow duplicate params as this would introduce problems with which one occurs last, and possible introduce security problems
        for(String tokenName : extractor.getTokens().keySet())
        {
            if (rv.containsKey(tokenName))
                throw new RuntimeException("Duplicate parameter " + tokenName);
            else
                rv.put(tokenName, extractor.getTokens().get(tokenName));
        }

        return rv;
    }


    /**
     * Gets a content-type based upon a feed type
     *
     * @param feedType the type of feed
     * @return a content type
     */
    private String getContentType(String feedType)
    {
        //figure out which content type to use. If we don't know, fall back to text/xml
        if (feedType.startsWith("atom"))
            return "application/atom+xml";
        else if (feedType.startsWith("rss"))
            return "application/rss+xml";
        else
            return "text/xml";
    }

    /**
     * Parse a string to integer
     * @param strToParse the string to parse
     * @param defaultValue the default value, if the string is failed to parse
     * @return integer
     */
    private int tryParseInt(String strToParse, int defaultValue)
    {
        try
        {
            return Integer.parseInt(strToParse);
        }
        catch (NumberFormatException ex)
        {
            return defaultValue;
        }
    }
}
