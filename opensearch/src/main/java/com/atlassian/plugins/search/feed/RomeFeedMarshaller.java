package com.atlassian.plugins.search.feed;

import com.atlassian.plugins.search.SearchResultsMarshaller;
import com.atlassian.plugins.util.XMLUtil;
import com.atlassian.sal.api.search.SearchMatch;
import com.atlassian.sal.api.search.SearchResults;
import com.google.common.base.Function;
import com.google.common.collect.Lists;
import com.sun.syndication.feed.synd.*;
import com.sun.syndication.io.FeedException;
import com.sun.syndication.io.SyndFeedOutput;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.Writer;
import java.text.MessageFormat;

/**
 * Results marshaller which uses Rome to create results as RSS/Atom feeds
 * https://rome.dev.java.net/
 */
public class RomeFeedMarshaller implements SearchResultsMarshaller
{
    private static final Logger log = LoggerFactory.getLogger(RomeFeedMarshaller.class);
    
    private String type;
    private int contentSize;

    private static final String TITLE_PATTERN = "Search results for query: {0}";
    private static final String DESCRIPTION_PATTERN = "Search results for query: {0}. Search returned {1} results in {2}ms";


    public RomeFeedMarshaller(String type, int contentSize)
    {
        this.type = type;
        this.contentSize = contentSize;
    }

    public void marshalTo(SearchResults searchResults, String baseUrl, String query, Writer writer) throws IOException
    {
        SyndFeed feed = new SyndFeedImpl();
        feed.setFeedType(type);
        feed.setLink(baseUrl);
        feed.setTitle(MessageFormat.format(TITLE_PATTERN, query));
        feed.setDescription(MessageFormat.format(DESCRIPTION_PATTERN, query, searchResults.getTotalResults(), searchResults.getSearchTime()));

        feed.setEntries(Lists.transform(searchResults.getMatches(), new ResultToFeedItemTransformer()));
        try
        {
            if (log.isDebugEnabled())
            {
                // This spits out the entire feed contents to the log; quite spammy. However, it's nice for proving to
                // people that Confluence has *actually* generated and marshalled the search results and the problem
                // is probably happening downstream.
                log.debug("Feed created: \r\n" + feed.toString());
            }
            new SyndFeedOutput().output(feed, writer);
        }
        catch (FeedException fe)
        {
            throw new IOException("Failed to create feed", fe);
        }
    }

    private class ResultToFeedItemTransformer implements Function<SearchMatch, SyndEntry>
    {
        public SyndEntry apply(SearchMatch match)
        {
            SyndEntryImpl entry = new SyndEntryImpl();
            entry.setTitle(match.getTitle());
            entry.setLink(match.getUrl());

            SyndContent content = new SyndContentImpl();
            content.setType("text/plain");

            // If negative value supply, only render title
            if (contentSize >= 0)
            {
                String matchContent = XMLUtil.replaceInvalidXmlCharacters(match.getExcerpt());

                // If content is empty, return the entry
                if (StringUtils.isBlank(matchContent))
                    return entry;

                // if original result is longer than specified, find the nearest full words and abbreviate on it
                if (contentSize >  0 && matchContent.length() > contentSize)
                   matchContent = StringUtils.abbreviate(matchContent, matchContent.indexOf(" ", contentSize) + 3);

                content.setValue(matchContent);
                entry.setDescription(content);
            }
            return entry;
        }
    }

}
