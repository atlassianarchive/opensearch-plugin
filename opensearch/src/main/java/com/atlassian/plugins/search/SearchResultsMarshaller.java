package com.atlassian.plugins.search;

import com.atlassian.sal.api.search.SearchResults;

import java.io.IOException;
import java.io.Writer;

/**
 * Marshaller to marshal search results
 */
public interface SearchResultsMarshaller
{
    /**
     * Marshals search results to a stream
     *
     * @param searchResults the results to Writer
     * @param writer        the writer to marshal to
     * @param query         the original query used to search
     * @param baseUrl       the base URL of the server returning the results
     * @throws IOException  if a problem occured writing to the supplied writer
     */
    void marshalTo(SearchResults searchResults, String baseUrl, String query, Writer writer) throws IOException;
}
