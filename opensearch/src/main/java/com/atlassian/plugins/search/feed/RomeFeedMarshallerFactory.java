package com.atlassian.plugins.search.feed;

import com.atlassian.plugins.search.SearchResultsMarshallerFactory;
import com.atlassian.plugins.search.SearchResultsMarshaller;

/**
 * Factory to create instances of a RomeFeedMarshaller
 */
public class RomeFeedMarshallerFactory implements SearchResultsMarshallerFactory
{
    /**
     * Creates a new RomeFeedMarshaller
     * @param type the feed type to create, such as rss_2.0
     * @return the marshaller
     */
    public SearchResultsMarshaller create(String type, int contentSize)
    {
        return new RomeFeedMarshaller(type, contentSize);
    }
}
