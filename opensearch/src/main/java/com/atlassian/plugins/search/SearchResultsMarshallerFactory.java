package com.atlassian.plugins.search;

/**
 * Factory interface for creating search results.
 */
public interface SearchResultsMarshallerFactory
{
    /**
     * Creates a search results marshaller
     * @param type the subtype of results to create
     * @return a marshaller
     */
    SearchResultsMarshaller create(String type, int contentSize);
}
