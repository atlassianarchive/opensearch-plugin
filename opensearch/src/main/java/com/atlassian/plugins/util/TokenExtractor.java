package com.atlassian.plugins.util;

import java.util.HashMap;
import java.util.Map;

/**
 *
 *
 */
public class TokenExtractor
{
    private static final String TOKEN_DELIM = "$$";
    private static final String KEY_VALUE_DELIM = "=";

    //supplied
    private String str;
    //derived
    private Map<String, String> tokens = new HashMap<String, String>();
    private String remaining;

    public TokenExtractor(String str)
    {
        this.str = str;
        parse();
    }

    private void parse()
    {
        int position = 0;
        StringBuilder remaining = new StringBuilder();

        while (true)
        {
            int tokenStart = str.indexOf(TOKEN_DELIM, position);

            if(tokenStart == -1)
                break;

            int keyValDelim = str.indexOf(KEY_VALUE_DELIM, tokenStart);
            int tokenEnd = str.indexOf(TOKEN_DELIM, tokenStart + TOKEN_DELIM.length());

            //only extract token if it's a valid one
            if(keyValDelim != -1 && tokenEnd != -1 && tokenEnd > keyValDelim)
            {
                tokens.put(str.substring(tokenStart + TOKEN_DELIM.length(), keyValDelim), str.substring(keyValDelim + KEY_VALUE_DELIM.length(), tokenEnd));
                remaining.append(str.substring(position, tokenStart));
                position = tokenEnd + TOKEN_DELIM.length();
            }
            else
            {
                remaining.append(str.substring(position, tokenStart + TOKEN_DELIM.length()));
                position = tokenStart + TOKEN_DELIM.length();
            }
        }

        remaining.append(str.substring(position, str.length()));
        this.remaining = remaining.toString();
    }
    

    public Map<String, String> getTokens()
    {
        return tokens;
    }

    public String getRemaining()
    {
        return remaining;
    }
}
