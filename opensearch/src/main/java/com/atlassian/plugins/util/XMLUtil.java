package com.atlassian.plugins.util;

/**
 * General XML Utility class.
 */
public class XMLUtil
{
    /**
     * Take a String which may contain characters outside of the XML character range (http://www.w3.org/TR/REC-xml/#charsets)
     * and return a String with those characters converted to Unicode "Replacement Character"s (0xFFFD).
     *
     * Note that this method does not "escape" the string (e.g. "&" is not converted to "&amp;").
     *
     * Similar to {@link org.jdom.Verifier#checkCharacterData(String)} but converts illegals instead of throwing Exceptions.
     * 
     * Stolen from Confluence core, so that it can be used cross-product
     */
    public static String replaceInvalidXmlCharacters(String text)
    {
        if (text == null) {
            return null;
        }

        char[] chars = text.toCharArray();

        // Do check
        for (int i = 0, len = text.length(); i<len; i++) {

            int ch = text.charAt(i);

            // Check if high part of a surrogate pair
            if (ch >= 0xD800 && ch <= 0xDBFF) {
                // Check if next char is the low-surrogate
                i++;
                if (i < len) {
                    char low = text.charAt(i);
                    if (low < 0xDC00 || low > 0xDFFF) {
                        chars[i - 1] = 0xFFFD;
                        chars[i] = 0xFFFD;
                        continue;
                    }
                    // It's a good pair, calculate the true value of
                    // the character to then fall thru to isXMLCharacter
                    ch = 0x10000 + (ch - 0xD800) * 0x400 + (low - 0xDC00);
                }
                else {
                    chars[i - 1] = 0xFFFD;
                    continue;
                }
            }

            if (!isXMLCharacter(ch)) {
                chars[i] = 0xFFFD;
            }
        }

        return String.valueOf(chars);
    }

    /**
     * Checks if a character is legal in XML.
     *
     * Based on http://www.w3.org/TR/MathML2/chapter6.html#chars.unicodechars
     */
    private static boolean isXMLCharacter(int c) {

        if (c == '\n' || c == '\r' || c == '\t') return true;

        if (c < 0x20) return false;
        if (c <= 0xD7FF) return true;

        if (c < 0xE000) return false;
        if (c <= 0xFFFD) return true;

        if (c < 0x10000) return false;
        return c <= 0x10FFFF;
    }
}
