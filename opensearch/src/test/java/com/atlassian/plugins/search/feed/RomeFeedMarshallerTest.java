package com.atlassian.plugins.search.feed;

import com.atlassian.sal.api.search.SearchMatch;
import com.atlassian.sal.api.search.SearchResults;
import com.google.common.collect.Lists;
import org.testng.annotations.Test;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Collections;
import java.util.regex.Pattern;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.*;


/**
 *
 *
 */
public class RomeFeedMarshallerTest
{
    @Test
    public void marshalEmptyResults() throws IOException
    {
        RomeFeedMarshaller toTest = new RomeFeedMarshaller("rss_2.0",0);
        StringWriter writer = new StringWriter();

        SearchResults results = mock(SearchResults.class);

        when(results.getMatches()).thenReturn(Collections.<SearchMatch>emptyList());
        when(results.getSearchTime()).thenReturn(7L);


        toTest.marshalTo(results, "test_base_url", "test_query", writer);

        String output = writer.toString();

        assertTrue(multilineFind(output, "<title>.*test_query.*</title>"));
        assertTrue(multilineFind(output, "<description>.*test_query.*</description>"));
        assertTrue(output.contains("7ms"));
        assertTrue(output.contains("<link>test_base_url</link>"));
        assertFalse(output.contains("<item>"));
    }

    @Test
    public void marshalSingleResult() throws IOException, XPathExpressionException, SAXException, ParserConfigurationException
    {
        RomeFeedMarshaller toTest = new RomeFeedMarshaller("rss_2.0",0);
        StringWriter writer = new StringWriter();

        SearchResults results = mock(SearchResults.class);

        SearchMatch match = mock(SearchMatch.class);

        when(results.getMatches()).thenReturn(Collections.singletonList(match));
        when(results.getSearchTime()).thenReturn(7L);

        when(match.getUrl()).thenReturn("match_url");
        when(match.getExcerpt()).thenReturn("match_excerpt");
        when(match.getTitle()).thenReturn("match_title");


        toTest.marshalTo(results, "test_base_url", "test_query", writer);

        String output = writer.toString();
        Document document = toDocument(output);

        XPathFactory factory = XPathFactory.newInstance();
        XPath xpath = factory.newXPath();

        assertEquals("1", xpath.evaluate("count(rss/channel/item)", document));
        assertTrue(multilineFind(output, "<title>match_title</title>"));
        assertTrue(multilineFind(output, "<description>match_excerpt</description>"));
        assertTrue(multilineFind(output, "<link>match_url</link>"));
    }

    @Test
    public void marshal9Results() throws IOException, XPathExpressionException, SAXException, ParserConfigurationException
    {
        RomeFeedMarshaller toTest = new RomeFeedMarshaller("rss_2.0",0);
        StringWriter writer = new StringWriter();

        SearchResults results = mock(SearchResults.class);

        SearchMatch match = mock(SearchMatch.class);

        when(results.getMatches()).thenReturn(Lists.newArrayList(match, match, match, match, match, match, match, match, match));
        when(results.getSearchTime()).thenReturn(7L);

        when(match.getUrl()).thenReturn("match_url");
        when(match.getExcerpt()).thenReturn("match_excerpt");
        when(match.getTitle()).thenReturn("match_title");


        toTest.marshalTo(results, "test_base_url", "test_query", writer);

        String output = writer.toString();
        Document document = toDocument(output);

        XPathFactory factory = XPathFactory.newInstance();
        XPath xpath = factory.newXPath();

        assertEquals("9", xpath.evaluate("count(rss/channel/item)", document));
    }

    Document toDocument(String document) throws ParserConfigurationException, IOException, SAXException
    {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true); // never forget this!
        DocumentBuilder builder = factory.newDocumentBuilder();
        return builder.parse(new InputSource(new StringReader(document)));
    }

    boolean multilineFind(String input, String regex)
    {
        Pattern pattern = Pattern.compile(regex, Pattern.MULTILINE);
        return pattern.matcher(input).find();
    }


}
