package com.atlassian.plugins.search.opensearch;

import com.atlassian.plugins.search.SearchResultsMarshaller;
import com.atlassian.plugins.search.SearchResultsMarshallerFactory;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.search.SearchProvider;
import com.atlassian.sal.api.search.SearchResults;
import com.atlassian.sal.api.user.UserManager;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.security.Principal;

import static org.mockito.Mockito.*;

/**
 *
 *
 */
public abstract class OpenSearchServletTest
{

    SearchProvider searchProvider;
    UserManager userManager;
    SearchResultsMarshallerFactory searchResultsMarshallerFactory;
    SearchResultsMarshaller searchResultsMarshaller;
    ApplicationProperties applicationProperties;
    HttpServletRequest req;
    HttpServletResponse resp;

    @BeforeMethod
    public void createMocks()
    {
        applicationProperties = mock(ApplicationProperties.class);
        searchResultsMarshaller = mock(SearchResultsMarshaller.class);
        userManager = mock(UserManager.class);
        searchProvider = mock(SearchProvider.class);
        searchResultsMarshallerFactory = mock(SearchResultsMarshallerFactory.class);
        req = mock(HttpServletRequest.class);
        resp = mock(HttpServletResponse.class);
    }

    protected OpenSearchServlet createServlet()
    {
        OpenSearchServlet toTest = new OpenSearchServlet(searchProvider, userManager, applicationProperties, searchResultsMarshallerFactory);
        when(searchResultsMarshallerFactory.create(anyString(),0)).thenReturn(searchResultsMarshaller);
        return toTest;
    }

    @Test
    public void useDefaultFeedType() throws Exception
    {
        when(req.getParameter(OpenSearchServlet.QUERY_PARAM)).thenReturn("test_query");

        OpenSearchServlet toTest = createServlet();
        toTest.doGet(req, resp);

        verify(searchResultsMarshallerFactory).create(OpenSearchServlet.DEFAULT_FEED_TYPE,0);
    }

    @Test
    public void correctParamsPassedtoMarshaller() throws Exception
    {
        SearchResults searchResults = mock(SearchResults.class);
        PrintWriter output = mock(PrintWriter.class);

        when(searchResultsMarshallerFactory.create("rss_2.0",0)).thenReturn(searchResultsMarshaller);
        when(searchProvider.search((String) eq(null), startsWith("test_query"))).thenReturn(searchResults);
        when(resp.getWriter()).thenReturn(output);
        when(req.getParameter(OpenSearchServlet.QUERY_PARAM)).thenReturn("test_query");
        when(applicationProperties.getBaseUrl()).thenReturn("base_url");

        OpenSearchServlet toTest = createServlet();
        toTest.doGet(req, resp);

        verify(searchResultsMarshaller).marshalTo(searchResults, "base_url", "test_query", output);
    }

    @Test
    public void adminImpersonateUser() throws Exception
    {
        when(req.getParameter(OpenSearchServlet.QUERY_PARAM)).thenReturn("test_query");
        when(req.getParameter(OpenSearchServlet.SHOULD_IMPERSONATE_PARAM)).thenReturn(Boolean.TRUE.toString());
        when(req.getParameter(OpenSearchServlet.IMPERSONATE_USER_PARAM)).thenReturn("other_user");

        doReturn("test_admin").when(userManager).getRemoteUsername();
        when(userManager.isSystemAdmin("test_admin")).thenReturn(true);
        Principal dummy = mock(Principal.class);
        when(userManager.resolve("other_user")).thenReturn(dummy);

        OpenSearchServlet toTest = createServlet();
        toTest.doGet(req, resp);

        verify(searchProvider).search("other_user", "test_query&startIndex=0&count=10");
    }

    @Test(expectedExceptions = SecurityException.class)
    public void nonAdminImpersonateUser() throws Exception
    {
        when(req.getParameter(OpenSearchServlet.QUERY_PARAM)).thenReturn("test_query");
        when(req.getParameter(OpenSearchServlet.SHOULD_IMPERSONATE_PARAM)).thenReturn(Boolean.TRUE.toString());
        when(req.getParameter(OpenSearchServlet.IMPERSONATE_USER_PARAM)).thenReturn("other_user");

        doReturn("test_nonadmin").when(userManager).getRemoteUsername();
        when(userManager.isSystemAdmin("test_nonadmin")).thenReturn(false);
        Principal dummy = mock(Principal.class);
        when(userManager.resolve("other_user")).thenReturn(dummy);

        OpenSearchServlet toTest = createServlet();
        toTest.doGet(req, resp);
    }

    @Test
    public void impersonateInvalidUser() throws Exception
    {
        when(req.getParameter(OpenSearchServlet.QUERY_PARAM)).thenReturn("test_query");
        when(req.getParameter(OpenSearchServlet.SHOULD_IMPERSONATE_PARAM)).thenReturn(Boolean.TRUE.toString());
        when(req.getParameter(OpenSearchServlet.IMPERSONATE_USER_PARAM)).thenReturn("other_user");

        doReturn("test_admin").when(userManager).getRemoteUsername();
        when(userManager.isSystemAdmin("test_admin")).thenReturn(true);

        when(userManager.resolve("other_user")).thenReturn(null);

        OpenSearchServlet toTest = createServlet();
        toTest.doGet(req, resp);

        verify(searchProvider).search("", "test_query&startIndex=0&count=10");
    }
}
