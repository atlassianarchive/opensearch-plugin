package com.atlassian.plugins.util;

import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

/**
 *
 *
 */
public class TokenExtractorTest
{
    @Test(expectedExceptions = NullPointerException.class)
    public void nullString()
    {
        new TokenExtractor(null);
    }

    @Test
    public void emptyString()
    {
        TokenExtractor extractor = new TokenExtractor("");
        assertEquals(extractor.getRemaining(), "");
        assertEquals(extractor.getTokens().size(), 0);
    }

    @Test
    public void noTokens()
    {
        TokenExtractor extractor = new TokenExtractor("test");
        assertEquals(extractor.getRemaining(), "test");
        assertEquals(extractor.getTokens().size(), 0);
    }


    @Test
    public void onlyOneToken()
    {
        TokenExtractor extractor = new TokenExtractor("$$1=test$$");
        assertEquals(extractor.getRemaining(), "");
        assertEquals(extractor.getTokens().size(), 1);
        assertEquals(extractor.getTokens().get("1"), "test");
    }

    @Test
    public void oneToken()
    {
        TokenExtractor extractor = new TokenExtractor("bl$$1=test$$ah");
        assertEquals(extractor.getRemaining(), "blah");
        assertEquals(extractor.getTokens().size(), 1);
        assertEquals(extractor.getTokens().get("1"), "test");
    }

    @Test
    public void onlyTwoTokens()
    {
        TokenExtractor extractor = new TokenExtractor("$$1=test1$$$$2=test2$$");
        assertEquals(extractor.getRemaining(), "");
        assertEquals(extractor.getTokens().size(), 2);
        assertEquals(extractor.getTokens().get("1"), "test1");
        assertEquals(extractor.getTokens().get("2"), "test2");
    }

    @Test
    public void twoTokens()
    {
        TokenExtractor extractor = new TokenExtractor("ar$$1=test1$$ou$$2=test2$$nd");
        assertEquals(extractor.getRemaining(), "around");
        assertEquals(extractor.getTokens().size(), 2);
        assertEquals(extractor.getTokens().get("1"), "test1");
        assertEquals(extractor.getTokens().get("2"), "test2");
    }

    @Test
    public void tokenMissingEquals()
    {
        TokenExtractor extractor = new TokenExtractor("test$$blah$$");
        assertEquals(extractor.getRemaining(), "test$$blah$$");
        assertEquals(extractor.getTokens().size(), 0);
    }

    @Test
    public void tokenMissingEnd()
    {
        TokenExtractor extractor = new TokenExtractor("test$$blah=b$");
        assertEquals(extractor.getRemaining(), "test$$blah=b$");
        assertEquals(extractor.getTokens().size(), 0);
    }

    @Test
    public void validTokenBetweenDollars()
    {
        TokenExtractor extractor = new TokenExtractor("test$$blah$$1=test$$$$");
        assertEquals(extractor.getRemaining(), "test$$blah$$");
        assertEquals(extractor.getTokens().size(), 1);
        assertEquals(extractor.getTokens().get("1"), "test");
    }

}
