package it.com.atlassian.plugins.search.opensearch;

import com.atlassian.confluence.plugin.functest.AbstractConfluencePluginWebTestCase;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.springframework.util.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.IOException;

/**
 *
 *
 */
public class OpenSearch1SearchTest extends AbstractConfluencePluginWebTestCase
{
    public void testAdminShouldSeeAllResults() throws Exception
    {
        testResults(buildUrl("testkeyword1", getConfluenceWebTester().getAdminUserName(), getConfluenceWebTester().getAdminPassword()), "testkeyword1", 2);
    }

    public void testUserShouldSeeSubsetOfResults() throws Exception
    {
        testResults(buildUrl("testkeyword1", "user", "user"), "testkeyword1", 1);
    }

    public void testUserCannotImpersonate() throws Exception
    {
        assertFailure(buildUrl("$$impersonate=admin$$testkeyword1", "user", "user"));
    }

    public void testAdminImpersonatingByUrlUserShouldSeeSubsetOfResults() throws Exception
    {
        testResults(buildUrl("testkeyword1", getConfluenceWebTester().getAdminUserName(), getConfluenceWebTester().getAdminPassword()) + "&impersonate=user", "testkeyword1", 1);
    }

    public void testAdminImpersonatingByKeywordUserShouldSeeSubsetOfResults() throws Exception
    {
        testResults(buildUrl("$$impersonate=user$$testkeyword1", getConfluenceWebTester().getAdminUserName(), getConfluenceWebTester().getAdminPassword()), "testkeyword1", 1);
    }

    public void testAdminDualImpersonationUserShouldSeeFailure() throws Exception
    {
        assertFailure(buildUrl("$$impersonate=user$$testkeyword1", getConfluenceWebTester().getAdminUserName(), getConfluenceWebTester().getAdminPassword()) + "&impersonate=user");
    }

    private Document loadDoc(GetMethod method) throws ParserConfigurationException, IOException, SAXException
    {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true);
        DocumentBuilder builder = factory.newDocumentBuilder();
        return builder.parse(method.getResponseBodyAsStream());
    }

    private String evaluateAsString(Node doc, String xpathExp) throws XPathExpressionException
    {
        return (String) evaluate(doc, xpathExp, XPathConstants.STRING);
    }

    private NodeList evaluateAsNodeList(Node doc, String xpathExp) throws XPathExpressionException
    {
        return (NodeList) evaluate(doc, xpathExp, XPathConstants.NODESET);
    }

    private Object evaluate(Node doc, String xpathExp, QName type) throws XPathExpressionException
    {
        XPathFactory factory = XPathFactory.newInstance();
        XPath xpath = factory.newXPath();
        return xpath.evaluate(xpathExp, doc, type);
    }

    protected String buildUrl(String query, String username, String password)
    {
        if (username == null)
            return getConfluenceWebTester().getBaseUrl() + "/plugins/servlet/opensearch?query=" + query;
        else
            return getConfluenceWebTester().getBaseUrl() + "/plugins/servlet/opensearch?query=" + query + "&os_username=" + username + "&os_password=" + password;
    }

    protected void assertFailure(String url) throws Exception
    {
        GetMethod method = new GetMethod(url);

        try
        {
            HttpClient httpClient = new HttpClient();
            assertEquals(500, httpClient.executeMethod(method));
        }
        finally
        {
            method.releaseConnection();
        }
    }



    protected void testResults(String url, String query, int expectedCount) throws Exception
    {
        GetMethod method = new GetMethod(url);

        try
        {
            HttpClient httpClient = new HttpClient();

            assertEquals(200, httpClient.executeMethod(method));
            assertEquals("application/rss+xml;charset=UTF-8", method.getResponseHeader("Content-Type").getValue());

            Document doc = loadDoc(method);

            assertResults(query, expectedCount, doc);

        }
        finally
        {
            method.releaseConnection();
        }
    }

    protected void assertResults(String query, int resultCount, Document doc) throws XPathExpressionException
    {
        String title = evaluateAsString(doc, "/rss/channel/title");
        assertTrue("Title must contain query expression", title.contains(query));

        String desc = evaluateAsString(doc, "/rss/channel/description");
        assertTrue("Description must contain query expression", desc.contains(query));

        NodeList nodes = evaluateAsNodeList(doc, "/rss/channel/item");

        assertEquals(resultCount, nodes.getLength());

        for (int i = 0; i < nodes.getLength(); i++)
        {
            Node node = nodes.item(i);
            assertHasNonEmptyChild(node, "title", i);
            assertHasNonEmptyChild(node, "link", i);
            //spaces have no description
            //assertHasNonEmptyChild(node, "description", i);
            assertHasNonEmptyChild(node, "guid", i);
        }
    }


    private void assertHasNonEmptyChild(Node node, String childName, int idx) throws XPathExpressionException
    {
        NodeList children = evaluateAsNodeList(node, childName);
        assertEquals("At index " + idx + ", must have one child element named " + childName, 1, children.getLength());
        assertTrue("At index " + idx + ", child  " + childName + " must have some text", StringUtils.hasText(children.item(0).getTextContent()));
    }

}