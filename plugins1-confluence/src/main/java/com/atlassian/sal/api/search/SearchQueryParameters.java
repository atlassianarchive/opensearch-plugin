package com.atlassian.minisal.api.search;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.Set;
import java.util.Collections;

import com.atlassian.minisal.api.search.SpaceCategoryEnum;
import com.atlassian.minisal.api.search.ContentTypeEnum;
import com.atlassian.minisal.api.search.query.DateRangeQuery;

/**
 * <p>
 * A bean encapsulating the query being issued to the search service.
 * </p>
 * <p>
 * All properties on this bean are optional. If not set then a property has no affect on the search.
 * </p>
 */
public class SearchQueryParameters
{
	private String query;
	private SpaceCategoryEnum category;
	private Set<String> spaceKeys;
	private ContentTypeEnum contentType;
	private DateRangeQuery.DateRange lastModified;
	private String contributor;

	/**
	 * Construct a query with no search term or filtering.
	 */
	public SearchQueryParameters()
	{
	}

	/**
	 * Construct a query for the specified String.
	 *
	 * @param query the String to search for.
	 */
	public SearchQueryParameters(String query)
	{
		this.query = query;
	}

	public void setQuery(String query)
	{
		this.query = query;
	}

	public void setCategory(SpaceCategoryEnum category)
	{
		if (category != null)
			this.category = category;
	}

	public void setSpaceKey(String spaceKey)
	{
		if (StringUtils.isNotBlank(spaceKey))
			this.spaceKeys = Collections.singleton(spaceKey);
	}

	public void setSpaceKeys(Set<String> spaceKeys)
	{
		this.spaceKeys = spaceKeys;
	}

	public void setContentType(ContentTypeEnum contentType)
	{
		this.contentType = contentType;
	}

	public void setLastModified(DateRangeQuery.DateRange lastModified)
	{
		this.lastModified = lastModified;
	}

	public void setContributor(String creator)
	{
		this.contributor = creator;
	}

	public String getQuery()
	{
		return query;
	}

	public SpaceCategoryEnum getCategory()
	{
		return category;
	}

	public Set<String> getSpaceKeys()
	{
		return spaceKeys;
	}

	public ContentTypeEnum getContentType()
	{
		return contentType;
	}

	public DateRangeQuery.DateRange getLastModified()
	{
		return lastModified;
	}

	public String getContributor()
	{
		return contributor;
	}

	public String toString()
	{
		return new ToStringBuilder(this)
			.append("query", query)
			.append("category", category)
			.append("spaceKeys", spaceKeys)
			.append("contentType", contentType)
			.append("lastModified", lastModified)
			.append("contributor", contributor)
			.toString();
	}
}
