package com.atlassian.minisal.api.search.query;

public enum BooleanOperator
{
	AND, OR
}
