package com.atlassian.minisal.api.search;

import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.Comment;
import com.atlassian.confluence.pages.BlogPost;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.user.PersonalInformation;
import com.atlassian.confluence.spaces.SpaceDescription;
import com.atlassian.confluence.mail.Mail;

import java.util.Map;
import java.util.HashMap;
import java.util.EnumSet;

/**
 * An enum representing the different content types that may be specified in a search.
 */
public enum ContentTypeEnum
{
	PAGE 											(Page.CONTENT_TYPE, Page.class.getName()),
	COMMENT 									(Comment.CONTENT_TYPE, Comment.class.getName()),
	BLOG 											(BlogPost.CONTENT_TYPE, BlogPost.class.getName()),
	ATTACHMENT 								(Attachment.CONTENT_TYPE, Attachment.class.getName()),
	PERSONAL_INFORMATION 			(PersonalInformation.CONTENT_TYPE, PersonalInformation.class.getName()),
	SPACE_DESCRIPTION 					(SpaceDescription.CONTENT_TYPE_SPACEDESC, SpaceDescription.class.getName()),
	PERSONAL_SPACE_DESCRIPTION 	(SpaceDescription.CONTENT_TYPE_PERSONAL_SPACEDESC, SpaceDescription.class.getName()),
    MAIL 											(Mail.CONTENT_TYPE, Mail.class.getName());
    //USER_STATUS                               (UserStatus.CONTENT_TYPE, UserStatus.class.getName());

	private static final Map<String, ContentTypeEnum> representationLookup = new HashMap<String, ContentTypeEnum>(8);
	private static final Map<String, ContentTypeEnum> classNameLookup = new HashMap<String, ContentTypeEnum>(8);

	static
	{
		 for(ContentTypeEnum contentTypeEnum : EnumSet.allOf(ContentTypeEnum.class))
         {
             representationLookup.put(contentTypeEnum.representation, contentTypeEnum);
             classNameLookup.put(contentTypeEnum.className, contentTypeEnum);
         }
	}

	private final String representation;
	private final String className;

    private ContentTypeEnum(String str, String c)
	{
		representation = str;
		className = c;
	}

	public String getRepresentation()
	{
		return representation;
	}

    public String getTypeClass()
    {
    	return className;
    }

    @Override
	public String toString()
    {
    	return getRepresentation();
    }

    /**
     * @deprecated since 3.0. Use {@link #getByRepresentation} or {@link #getByClassName} instead.
	 */
    @Deprecated
	public static ContentTypeEnum get(String representation)
	{
		return getByRepresentation(representation);
	}

    /**
     * @param representation the String representation of the enum. e.g. {@link Page#CONTENT_TYPE}
     * @return the ContentTypeEnum for the supplied String. null is returned if the parameter does not map to a
     * ContentTypeEnum.
     */
    public static ContentTypeEnum getByRepresentation(String representation)
    {
        return representationLookup.get(representation);
    }

    /**
     * @param className fully qualified class name e.g. "com.atlassian.confluence.pages.Page"
     * @return the ContentTypeEnum for the supplied class name. null if none found.
     */
    public static ContentTypeEnum getByClassName(String className)
    {
        return classNameLookup.get(className);
    }
}
