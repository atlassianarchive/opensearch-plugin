package com.atlassian.minisal.api.search;

import com.atlassian.minisal.api.search.query.DateRangeQuery;

import java.util.Date;

/**
 * Enum representing the 'friendly' date ranges presented on the criteria filter on the search results screen.
 */
public enum DateRangeEnum
{
	LASTDAY(1),
	LASTTWODAYS(2),
	LASTWEEK(7),
	LASTMONTH(31),
    LASTSIXMONTHS(365 / 2),
    LASTYEAR(365),
    LASTTWOYEARS(365 * 2);

	private final long millis;

    DateRangeEnum(long days)
	{
        millis = days * 1000 * 60 * 60 * 24;
	}

	public DateRangeQuery.DateRange dateRange()
	{
        Date startDate = new Date(System.currentTimeMillis() - millis);
		return new DateRangeQuery.DateRange(startDate, null, true, false);
	}
}
