package com.atlassian.minisal.api.search;

import bucket.core.actions.PaginationSupport;
import com.atlassian.bonnie.AnyTypeObjectDao;
import com.atlassian.bonnie.Searcher;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.labels.LabelManager;
import com.atlassian.confluence.search.actions.SearchBean;
import com.atlassian.confluence.search.actions.SearchQueryBean;
import com.atlassian.confluence.search.actions.SearchResultWithExcerpt;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.plugins.util.Check;
import com.atlassian.sal.api.search.ResourceType;
import com.atlassian.sal.api.search.SearchMatch;
import com.atlassian.sal.api.search.SearchProvider;
import com.atlassian.sal.api.search.SearchResults;
import com.atlassian.spring.container.ContainerManager;
import com.atlassian.user.User;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 *
 */
public class MiniSearchProvider implements SearchProvider, InitializingBean
{
    private SearchQueryBean searchQueryBean;
    private AnyTypeObjectDao anyTypeObjectDao;
    private Searcher searcher;
    private SpaceManager spaceManager;
    private UserAccessor userAccessor;
    private LabelManager labelManager;
    private SettingsManager settingsManager;

    public void afterPropertiesSet() throws Exception
    {
        Check.notNull(anyTypeObjectDao, "anyTypeObjectDao");
        searchQueryBean = new SearchQueryBean(searcher, spaceManager, userAccessor, labelManager, settingsManager);
        ContainerManager.autowireComponent(searchQueryBean);
    }

    public SearchResults search(String username, String searchQuery)
    {
        String[] queryParams = searchQuery.split("&");

        if (queryParams.length == 0 || !StringUtils.hasText(queryParams[0]))
            throw new IllegalArgumentException("Query cannot be blank");

        String query = queryParams[0];

        int count = 10;
        int start = 0;

        for (int i = 1; i < queryParams.length; i++)
        {
            String[] keyAndValue = queryParams[i].split("=");
            if (keyAndValue.length != 2)
                throw new IllegalArgumentException("Query contains bad parameter; must be key and value: " + queryParams[i]);

            if ("startIndex".equals(keyAndValue[0]))
                start = Integer.parseInt(keyAndValue[1]);
            else if ("maxhits".equals(keyAndValue[0]))
                count = Integer.parseInt(keyAndValue[1]);
            else
                throw new IllegalArgumentException("Query contains unrecognised parameter: " + queryParams[i]);
        }

        try
        {
            searchQueryBean.setQueryString(query);
        }
        catch (IOException ioe)
        {
            throw new RuntimeException("Failed to set query", ioe);
        }


        long searchTime;
        List<SearchResultWithExcerpt> searchResults;

        User authenticatedUser = AuthenticatedUserThreadLocal.getUser();
        User passedUser = userAccessor.getUser(username);

        try
        {
            AuthenticatedUserThreadLocal.setUser(passedUser);
            SearchBean sb = new SearchBean();
            ContainerManager.autowireComponent(sb);
            PaginationSupport paginationSupport = new PaginationSupport(count);
            paginationSupport.setStartIndex(start);
            sb.setPaginationSupport(paginationSupport);
            sb.setAnyTypeObjectDao(anyTypeObjectDao);
            long startTime = System.currentTimeMillis();
            sb.search(searchQueryBean.buildQuery());
            searchTime = System.currentTimeMillis() - startTime;
            searchResults = paginationSupport.getPage();
        }
        finally
        {
            AuthenticatedUserThreadLocal.setUser(authenticatedUser);
        }

        List<SearchMatch> matches = new ArrayList<SearchMatch>(searchResults.size());

        //there shouldn't be many search results, and filtering & checking them would be far more complex, so just buffer them here
        for (SearchResultWithExcerpt srwe : searchResults)
        {
            Object o = srwe.getResultObject();

            if (o != null && o instanceof ContentEntityObject)
                matches.add(convert((ContentEntityObject) o));
        }

        return new SearchResults(matches, searchResults.size(), searchTime);
    }

    protected SearchMatch convert(final ContentEntityObject ceo)
    {
        return new SearchMatch()
        {
            public String getUrl()
            {
                return ceo.getUrlPath();
            }

            public String getTitle()
            {
                return ceo.getTitle();
            }

            public String getExcerpt()
            {
                return ceo.getExcerpt();
            }

            public ResourceType getResourceType()
            {
                throw new UnsupportedOperationException();
            }
        };
    }

    public void setAnyTypeObjectDao(AnyTypeObjectDao anyTypeObjectDao)
    {
        this.anyTypeObjectDao = anyTypeObjectDao;
    }

    public void setSearcher(Searcher searcher)
    {
        this.searcher = searcher;
    }

    public void setSpaceManager(SpaceManager spaceManager)
    {
        this.spaceManager = spaceManager;
    }

    public void setUserAccessor(UserAccessor userAccessor)
    {
        this.userAccessor = userAccessor;
    }

    public void setLabelManager(LabelManager labelManager)
    {
        this.labelManager = labelManager;
    }

    public void setSettingsManager(SettingsManager settingsManager)
    {
        this.settingsManager = settingsManager;
    }
}
