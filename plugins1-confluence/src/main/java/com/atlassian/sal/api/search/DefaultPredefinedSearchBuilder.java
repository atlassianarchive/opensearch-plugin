//package com.atlassian.minisal.api.search;
//
//import com.atlassian.confluence.search.v2.SearchQuery;
//import com.atlassian.confluence.search.v2.Search;
//import com.atlassian.confluence.search.v2.sort.ModifiedSort;
//import com.atlassian.confluence.search.v2.query.AllQuery;
//import com.atlassian.confluence.search.v2.query.ContentTypeQuery;
//import com.atlassian.confluence.search.v2.query.InSpaceQuery;
//import com.atlassian.confluence.search.v2.filter.SubsetResultFilter;
//
//import org.apache.commons.lang.StringUtils;
//
//import java.util.List;
//import java.util.ArrayList;
//import java.util.Set;
//import java.util.HashSet;
//
//public class DefaultPredefinedSearchBuilder
//{
//	private final SearchFilter permissionsSearchFilter;
//
//    public DefaultPredefinedSearchBuilder()
//	{
//		this.permissionsSearchFilter = SiteSearchPermissionsSearchFilter.getInstance();
//    }
//
//    public ISearch buildSiteSearch(SearchQueryParameters searchQueryParams, int startIndex, int pageSize)
//    {
//        if (searchQueryParams == null)
//            throw new IllegalArgumentException("searchQueryParams cannot be null");
//        if (startIndex < 0)
//            throw new IllegalArgumentException("Start index must be greater than 0");
//
//        SearchQuery query = siteSearchQuery(searchQueryParams);
//        return new ContentSearch(query, null, permissionsSearchFilter, new SubsetResultFilter(startIndex, pageSize));
//    }
//
//    public ISearch buildUsersSearch(String query, int maxResults)
//    {
//        if (maxResults <= 0)
//            throw new IllegalArgumentException("maxResults must be greater than 0");
//
//        if (StringUtils.isBlank(query))
//            throw new IllegalArgumentException("query String must be specified");
//
//        return new ContentSearch(AllQuery.getInstance(), null, new AuthorSearchFilter(query), new SubsetResultFilter(0, maxResults));
//    }
//
//    public ISearch buildRecentUpdateSearch(RecentUpdateQueryParameters params, int startIndex, int numberOfResults)
//    {
//        if (params == null)
//            throw new IllegalArgumentException("params cannot be null");
//        if (startIndex < 0)
//            throw new IllegalArgumentException("Start index must be greater than 0");
//
//        List<SearchFilter> searchFilters = new ArrayList<SearchFilter>();
//        searchFilters.add(SiteSearchPermissionsSearchFilter.getInstance());
//
//        if (params.getUsernames() != null && !params.getUsernames().isEmpty())
//            searchFilters.add(new LastModifierSearchFilter(params.getUsernames().toArray(new String[params.getUsernames().size()])));
//
//        if (params.getLabels() != null && !params.getLabels().isEmpty())
//            searchFilters.add(new LabelsSearchFilter(params.getLabels()));
//
//        if (params.getSpaceKeys() != null && !params.getSpaceKeys().isEmpty())
//            searchFilters.add(new InSpaceSearchFilter(params.getSpaceKeys()));
//
//        SearchQuery searchQuery = AllQuery.getInstance();
//        if (params.getContentTypes() != null && !params.getContentTypes().isEmpty())
//            searchQuery = new ContentTypeQuery(params.getContentTypes());
//
//        final ChainedSearchFilter searchFilter = new ChainedSearchFilter(searchFilters, ChainedSearchFilter.Operator.AND);
//        return new ChangesSearch(searchQuery, ModifiedSort.DEFAULT, searchFilter, new SubsetResultFilter(startIndex, numberOfResults));
//    }
//
//    private SearchQuery siteSearchQuery(SearchQueryParameters searchQueryParams)
//	{
//		Set<SearchQuery> scopedQuery = new HashSet<SearchQuery>();
//
//		if (StringUtils.isBlank(searchQueryParams.getQuery()))
//			scopedQuery.add(new AllQuery());
//		else
//            scopedQuery.add(new MultiTextFieldQuery(searchQueryParams.getQuery(), "title", "labelText", "contentBody", "filename", "username", "fullName", "email", "from", "recipients"));
//
//		if (searchQueryParams.getContentType() != null)
//			scopedQuery.add(new ContentTypeQuery(searchQueryParams.getContentType()));
//
//        if (searchQueryParams.getLastModified() != null)
//			scopedQuery.add(new DateRangeQuery(searchQueryParams.getLastModified(), DateRangeQuery.DateRangeQueryType.MODIFIED));
//
//		if (searchQueryParams.getCategory() != null)
//			scopedQuery.add(new SpaceCategoryQuery(searchQueryParams.getCategory()));
//
//		if (searchQueryParams.getSpaceKeys() != null && !searchQueryParams.getSpaceKeys().isEmpty())
//			scopedQuery.add(new InSpaceQuery(searchQueryParams.getSpaceKeys()));
//
//		if (StringUtils.isNotBlank(searchQueryParams.getContributor()))
//			scopedQuery.add(contributorQuery(searchQueryParams.getContributor()));
//
//		BooleanQuery query = new BooleanQuery(scopedQuery, null, null);
//
//        return new BoostingQuery(query, searchQueryParams);
//	}
//
//    /**
//     * @deprecated See super class javadoc.
//     */
//    public Search siteSearch(SearchQueryParameters searchQueryParams, int startIndex, int pageSize)
//    {
//        SearchQuery query = siteSearchQuery(searchQueryParams);
//        return new Search(query, null, new SubsetResultFilter(startIndex, pageSize));
//    }
//}
