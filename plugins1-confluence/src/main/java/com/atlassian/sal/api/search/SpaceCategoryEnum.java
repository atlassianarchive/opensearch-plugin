package com.atlassian.minisal.api.search;

import java.util.Map;
import java.util.HashMap;
import java.util.EnumSet;

/**
 * An enum representing the different categories of space that may be specified in a search.
 */
public enum SpaceCategoryEnum
{
    ALL         ("conf_all"),
    FAVOURITES  ("conf_favorites"),
    GLOBAL      ("conf_global"),
    PERSONAL    ("conf_personal");

    private static final Map<String, SpaceCategoryEnum> lookup = new HashMap<String, SpaceCategoryEnum>(SpaceCategoryEnum.values().length);

    static
    {
        for(SpaceCategoryEnum cat : EnumSet.allOf(SpaceCategoryEnum.class))
            lookup.put(cat.getRepresentation(), cat);
    }

    private final String rerepresentation;

    private SpaceCategoryEnum(String str)
    {
        rerepresentation = str;
    }

    public String getRepresentation()
    {
        return rerepresentation;
    }

    /**
     * The String representation of the enum.
     */
    @Override
    public String toString()
    {
        return getRepresentation();
    }

    /**
     * @param rep the String representation of the enum.
     * @return the SpaceCategoryEnum for the supplied String. Null is returned if the parameter does not map to a
     * SpaceCategoryEnum.
     */
    public static SpaceCategoryEnum get(String rep)
    {
        return lookup.get(rep);
    }
}
