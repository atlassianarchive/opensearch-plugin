package com.atlassian.minisal.api;

import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.plugins.util.Check;

import java.util.Date;
import java.io.File;

/**
 *
 *
 */
public class MiniApplicationProperties implements ApplicationProperties
{
    private SettingsManager settingsManager;

    public void setSettingsManager(SettingsManager settingsManager)
    {
        this.settingsManager = settingsManager;
    }

    public String getBaseUrl()
    {
        return settingsManager.getGlobalSettings().getBaseUrl();
    }

    public String getDisplayName()
    {
        throw new UnsupportedOperationException();
    }

    public String getVersion()
    {
        throw new UnsupportedOperationException();
    }

    public Date getBuildDate()
    { 
        throw new UnsupportedOperationException();
    }

    public String getBuildNumber()
    {
        throw new UnsupportedOperationException();
    }

    public File getHomeDirectory()
    {
        throw new UnsupportedOperationException();
    }
}
