package com.atlassian.minisal.api.user;

import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.user.UserResolutionException;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.user.User;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;

/**
 *
 *
 */
public class MiniUserManager implements UserManager
{
    private UserAccessor userAccessor;

    public String getRemoteUsername()
    {
        return AuthenticatedUserThreadLocal.getUsername();
    }

    public String getRemoteUsername(HttpServletRequest request)
    {
        throw new UnsupportedOperationException();
    }

    public boolean isUserInGroup(String username, String group)
    {
        throw new UnsupportedOperationException();
    }

    public boolean isSystemAdmin(String username)
    {
        User user = userAccessor.getUser(username);

        if(user == null)
            throw new IllegalArgumentException("User " + username + " does not exist");

        return userAccessor.isSuperUser(user);
    }

    public boolean authenticate(String username, String password)
    {
        throw new UnsupportedOperationException();
    }

    public Principal resolve(String username) throws UserResolutionException
    {
        return userAccessor.getUser(username);
    }

    public void setUserAccessor(UserAccessor userAccessor)
    {
        this.userAccessor = userAccessor;
    }
}
