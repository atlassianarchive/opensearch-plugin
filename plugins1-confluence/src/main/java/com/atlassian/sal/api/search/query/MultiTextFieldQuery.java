package com.atlassian.minisal.api.search.query;

import com.atlassian.confluence.search.v2.SearchQuery;

import java.util.*;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;


public class MultiTextFieldQuery implements SearchQuery
{
    private static final String KEY = "multiTextField";

    private final String query;
    private final Set<String> fields;
    private final BooleanOperator operator;

    /**
     * Constructs a multi-field query with the specified query, fields and operator.
     *
     * @param query
     * @param fields
     * @param operator
     */
    public MultiTextFieldQuery(String query, Set<String> fields, BooleanOperator operator)
    {
        if (StringUtils.isBlank(query))
            throw new IllegalArgumentException("query parameter is required.");

        if ((fields == null) || (fields.isEmpty()))
            throw new IllegalArgumentException("fields parameter is required");

        if (operator == null)
            throw new IllegalArgumentException("operator parameter is required");

        this.query = query;
        this.fields = fields;
        this.operator = operator;
    }

    /**
     * Constructs a multi-field query with the operator set to {@link BooleanOperator#AND}.
     * @param query the query
     * @param fields the fields to query against
     */
    public MultiTextFieldQuery(String query, String... fields)
    {
        if (StringUtils.isBlank(query))
            throw new IllegalArgumentException("query parameter is required.");

        if ((fields == null) || (fields.length == 0))
            throw new IllegalArgumentException("fields parameter is required");

        this.query = query;

        this.fields = new HashSet<String>(fields.length);
        Collections.addAll(this.fields, fields);

        this.operator = BooleanOperator.AND;
    }

    public String getKey()
    {
        return KEY;
    }

    public String getQuery()
    {
        return query;
    }

    public Set<String> getFields()
    {
        return new HashSet<String>(fields);
    }

    public BooleanOperator getOperator()
    {
        return operator;
    }

    public List getParameters()
    {
        return Arrays.asList(query, fields);
    }

	@Override
	public boolean equals(Object obj)
	{
		if (obj == null)
			return false;

		if (obj == this)
			return true;

		if (obj.getClass() != getClass())
			return false;

		MultiTextFieldQuery other = (MultiTextFieldQuery)obj;

		return new EqualsBuilder().append(query, other.query).append(fields, other.fields).append(operator, other.operator).isEquals();
	}

	@Override
	public int hashCode()
	{
		return new HashCodeBuilder(171, 43).append(query).append(fields).append(operator).toHashCode();
	}
}