package com.atlassian.minisal.api.search.query;

import com.atlassian.confluence.search.v2.SearchQuery;

import java.util.Date;
import java.util.List;
import java.util.Arrays;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class DateRangeQuery implements SearchQuery
{
	public enum DateRangeQueryType
	{
		MODIFIED
	}

	private static final String KEY = "dateRange";

	private final DateRange dateRange;
	private DateRangeQueryType dateRangeQueryType;

	/**
	 * Query for search results that have been modified within the specified range of dates.
	 *
	 * @param from the beginning of the date range
	 * @param to the end date in the range
	 * @param includeFrom include the beginning date in the range
	 * @param includeTo include the end date in the range
	 * @param dateRangeQueryType the type of range query. See
	 *            {@link com.atlassian.confluence.search.v2.query.DateRangeQuery.DateRangeQueryType}
	 */
	public DateRangeQuery(Date from, Date to, boolean includeFrom, boolean includeTo,
			DateRangeQueryType dateRangeQueryType)
	{
		this(new DateRange(from, to, includeFrom, includeTo), dateRangeQueryType);
	}

	/**
	 * Query for search results that have been modified within the specified range of dates.
	 *
	 * @param dateRange the dateRange. See {@link com.atlassian.confluence.search.v2.query.DateRangeQuery.DateRange}
	 * @param dateRangeQueryType the type of range query. See
	 *            {@link com.atlassian.confluence.search.v2.query.DateRangeQuery.DateRangeQueryType}
	 */
	public DateRangeQuery(DateRange dateRange, DateRangeQueryType dateRangeQueryType)
	{
		this.dateRange = dateRange;
		this.dateRangeQueryType = dateRangeQueryType;
	}

	public String getKey()
	{
		return KEY;
	}

	public List getParameters()
	{
		return Arrays.asList(dateRange.getFrom(), dateRange.getTo(), dateRange.isIncludeFrom(),
				dateRange.isIncludeTo(), dateRangeQueryType);
	}

	public Date getFromDate()
	{
		return dateRange.getFrom();
	}

	public Date getToDate()
	{
		return dateRange.getTo();
	}

	public boolean isIncludeFrom()
	{
		return dateRange.isIncludeFrom();
	}

	public boolean isIncludeTo()
	{
		return dateRange.isIncludeTo();
	}

	public DateRangeQueryType getDateRangeQueryType()
	{
		return dateRangeQueryType;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (obj == null)
			return false;

		if (obj == this)
			return true;

		if (obj.getClass() != getClass())
			return false;

		DateRangeQuery other = (DateRangeQuery)obj;
		return new EqualsBuilder().append(dateRangeQueryType, other.dateRangeQueryType).append(dateRange, other.dateRange).isEquals();
	}

	@Override
	public int hashCode()
	{
		return new HashCodeBuilder(111, 37).append(dateRangeQueryType).append(dateRange).toHashCode();
	}

	/**
	 * Class to represent a date range. Inclusion/exclusion of bounds is settable. Immutable.
	 */
	public static class DateRange
	{
		private final Date from;
		private final Date to;
		private final boolean includeFrom;
		private final boolean includeTo;

		public DateRange(Date from, Date to, boolean includeFrom, boolean includeTo)
		{
			this.from = from;
			this.to = to;
			this.includeFrom = includeFrom;
			this.includeTo = includeTo;
		}

		public Date getFrom()
		{
			if (from == null)
				return null;

			return new Date(from.getTime());
		}

		public Date getTo()
		{
			if (to == null)
				return null;

			return new Date(to.getTime());
		}

		public boolean isIncludeFrom()
		{
			return includeFrom;
		}

		public boolean isIncludeTo()
		{
			return includeTo;
		}

		@Override
		public boolean equals(Object obj)
		{
			if (obj == null)
				return false;

			if (obj == this)
				return true;

			if (obj.getClass() != getClass())
				return false;

			DateRange other = (DateRange) obj;
			return new EqualsBuilder().append(from, other.from).append(to, other.to).append(includeFrom,
					other.includeFrom).append(includeTo, other.includeTo).isEquals();
		}

		@Override
		public int hashCode()
		{
			return new HashCodeBuilder(113, 37).append(from).append(to).append(includeFrom).append(includeTo).toHashCode();
		}
	}
}