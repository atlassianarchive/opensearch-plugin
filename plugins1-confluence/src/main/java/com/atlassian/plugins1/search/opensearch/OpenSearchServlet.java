package com.atlassian.plugins1.search.opensearch;

import com.atlassian.plugins.search.SearchResultsMarshallerFactory;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.search.SearchProvider;
import com.atlassian.sal.api.user.UserManager;
import org.springframework.beans.factory.InitializingBean;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;
import java.io.IOException;

/**
 * Wrapper for a normal OpenSearchServlet; uses setter injection as constructor injection not available for plugins1.
 *
 */
public class OpenSearchServlet extends HttpServlet implements InitializingBean
{
    private SearchProvider searchProvider;
    private UserManager userManager;
    private ApplicationProperties applicationProperties;
    private SearchResultsMarshallerFactory searchResultsMarshallerFactory;

    private com.atlassian.plugins.search.opensearch.OpenSearchServlet plugins2Servlet;

    /**
     * Particularly nasty hack due to Spring injecting dependencies but nothing else (no constructor injection or afterPropertiesSet())
     */
    public void maybeAllSet()
    {
        if(searchProvider != null && userManager != null && applicationProperties != null && searchResultsMarshallerFactory != null)
            try
            {
                afterPropertiesSet();
            }
            catch (Exception e)
            {
                throw new RuntimeException(e);
            }
    }

    public void afterPropertiesSet() throws Exception
    {
        plugins2Servlet = new com.atlassian.plugins.search.opensearch.OpenSearchServlet(
                searchProvider,
                userManager,
                applicationProperties,
                searchResultsMarshallerFactory
        );
    }

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        plugins2Servlet.service(req, resp);
    }

    public void setMiniSearchProvider(SearchProvider searchProvider)
    {
        this.searchProvider = searchProvider;
        maybeAllSet();
    }

    public void setMiniUserManager(UserManager userManager)
    {
        this.userManager = userManager;
        maybeAllSet();
    }

    public void setMiniApplicationProperties(ApplicationProperties applicationProperties)
    {
        this.applicationProperties = applicationProperties;
        maybeAllSet();
    }

    public void setSearchResultsMarshallerFactory(SearchResultsMarshallerFactory searchResultsMarshallerFactory)
    {
        this.searchResultsMarshallerFactory = searchResultsMarshallerFactory;
        maybeAllSet();
    }
}
